package com.ncteam.mavenintegrationwithpipeline.controller;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ncteam.mavenintegrationwithpipeline.bean.SampleBean;

@RestController
public class SampleController {

	@GetMapping("/samplebean")
	public SampleBean getBean() {
		return new SampleBean("System User", LocalDateTime.now());
	}
	@GetMapping("/samplebeanpath")
	public SampleBean getBeanPath() {
		return new SampleBean("another User", LocalDateTime.now());
	}
}
