package com.ncteam.mavenintegrationwithpipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MavenIntegrationWithPipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(MavenIntegrationWithPipelineApplication.class, args);
	}

}
