package com.ncteam.mavenintegrationwithpipeline.bean;

import java.time.LocalDateTime;

public class SampleBean {

	private String userName;
	private LocalDateTime loginTime;
	
	@Override
	public String toString() {
		return "SampleBean [userName=" + userName + ", loginTime=" + loginTime + "]";
	}
	public SampleBean(String userName, LocalDateTime loginTime) {
		super();
		this.userName = userName;
		this.loginTime = loginTime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public LocalDateTime getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(LocalDateTime loginTime) {
		this.loginTime = loginTime;
	}
	
	
	
}
