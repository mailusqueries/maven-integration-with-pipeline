FROM openjdk:8
EXPOSE 8085
ADD target/maven-integration-with-pipeline.jar maven-integration-with-pipeline.jar
ENTRYPOINT ["java","-jar","/maven-integration-with-pipeline.jar"]